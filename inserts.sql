﻿insert into turma values(3, 'Informática', 3);
insert into turma values(1, 'Informática', 1);
insert into turma values(2, 'Informática', 2);
insert into turma values(4, 'Informática', 4);
insert into turma values(5, 'Administração', 1);
insert into turma values(6, 'Administração', 2);
insert into turma values(7, 'Administração', 3);
insert into turma values(8, 'Administração', 4);
insert into turma values(9, 'Eletrônica', 1);
insert into turma values(10, 'Eletrônica', 2);
insert into turma values(11, 'Eletrônica', 3);
insert into turma values(12, 'Eletrônica', 4);

insert into disciplina values(1, 'Banco de Dados');
insert into disciplina values(2, 'Matemática');
insert into disciplina values(3, 'Sociologia');
insert into disciplina values(4, 'Português');
insert into disciplina values(5, 'Química');
insert into disciplina values(6, 'Programação');
insert into disciplina values(7, 'Análise e Projeto de Sistemas');
insert into disciplina values(8, 'Filosofia');
insert into disciplina values(9, 'Biologia');
insert into disciplina values(10, 'Física');
insert into disciplina values(11, 'História e Geografia');

insert into turma_disciplina values(3, 1);
insert into turma_disciplina values(3, 2);
insert into turma_disciplina values(3, 3);
insert into turma_disciplina values(3, 4);
insert into turma_disciplina values(3, 5);
insert into turma_disciplina values(3, 6);
insert into turma_disciplina values(3, 7);
insert into turma_disciplina values(3, 8);
insert into turma_disciplina values(3, 9);
insert into turma_disciplina values(3, 10);
insert into turma_disciplina values(3, 11);

