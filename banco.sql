
/* Drop Tables */

DROP TABLE IF EXISTS nota_atigindida;
DROP TABLE IF EXISTS nota_necessaria;
DROP TABLE IF EXISTS aluno;
DROP TABLE IF EXISTS turma_disciplina;
DROP TABLE IF EXISTS disciplina;
DROP TABLE IF EXISTS turma;




/* Create Tables */

CREATE TABLE aluno
(
	id_aluno serial NOT NULL UNIQUE,
	nome_aluno varchar NOT NULL,
	email_aluno varchar NOT NULL UNIQUE,
	senha_aluno varchar NOT NULL,
	id_turma int NOT NULL,
	PRIMARY KEY (id_aluno)
) WITHOUT OIDS;


CREATE TABLE disciplina
(
	id_disciplina serial NOT NULL UNIQUE,
	nome_disciplina varchar UNIQUE,
	PRIMARY KEY (id_disciplina)
) WITHOUT OIDS;


CREATE TABLE nota_atigindida
(
	id_aluno int NOT NULL,
	id_disciplina int NOT NULL,
	primeiro_trimestre float DEFAULT null,
	segundo_trimestre float DEFAULT null,
	terceiro_trimestre float DEFAULT null,
	PRIMARY KEY (id_aluno, id_disciplina)
) WITHOUT OIDS;


CREATE TABLE nota_necessaria
(
	id_aluno int NOT NULL,
	id_disciplina int NOT NULL,
	segundo_trimestre float DEFAULT null,
	terceiro_trimestre float DEFAULT null,
	PRIMARY KEY (id_aluno, id_disciplina)
) WITHOUT OIDS;


CREATE TABLE turma
(
	id_turma serial NOT NULL UNIQUE,
	curso varchar NOT NULL,
	ano int NOT NULL,
	PRIMARY KEY (id_turma)
) WITHOUT OIDS;


CREATE TABLE turma_disciplina
(
	id_turma int NOT NULL,
	id_disciplina int NOT NULL,
	PRIMARY KEY (id_turma, id_disciplina)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE nota_atigindida
	ADD FOREIGN KEY (id_aluno)
	REFERENCES aluno (id_aluno)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nota_necessaria
	ADD FOREIGN KEY (id_aluno)
	REFERENCES aluno (id_aluno)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nota_atigindida
	ADD FOREIGN KEY (id_disciplina)
	REFERENCES disciplina (id_disciplina)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE nota_necessaria
	ADD FOREIGN KEY (id_disciplina)
	REFERENCES disciplina (id_disciplina)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE turma_disciplina
	ADD FOREIGN KEY (id_disciplina)
	REFERENCES disciplina (id_disciplina)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE aluno
	ADD FOREIGN KEY (id_turma)
	REFERENCES turma (id_turma)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE turma_disciplina
	ADD FOREIGN KEY (id_turma)
	REFERENCES turma (id_turma)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



