<?php 
session_start();
if(isset($_SESSION['idLogin'])){
    header("Location: home.php");
}
?>
<!DOCTYPE HTML>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
	<title>Cadastro</title>
	<link rel="stylesheet" href="css/materialize.css">
	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> </head>

<body class="light-green lighten-5">
	<header>
            <div class="navbar-fixed">
		<nav class="light-green lighten-1" role="navigation">
			<div class="nav-wrapper">
				<a href="#!" class="brand-logo center"><img class="logo" src="img/ifrs-logo.svg"></a>
			</div>
		</nav>
            </div>
	</header>
	<div class="row">
		<div class="div-cadastro">
                    <form method="POST" action="db/cadastrarAluno.php" class="col s12">
                <div class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">account_circle</i>
						<input id="nome" name="nome" type="text" class="validate" required>
						<label for="nome">Nome</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">email</i>
						<input id="email" name="email" type="email" class="validate" required>
						<label for="email">Email</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">lock</i>
						<input id="senha" name="senha" type="password" class="validate" required>
						<label for="senha">Senha</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s6">
						<select name="curso" class="browser-default" required>
                                                    <optgroup label="Curso">
                                                        <option value="Administração">Administração</option>
                                                        <option value="Eletrônica">Eletrônica</option>
                                                        <option value="Informática">Informática</option>
                                                    </optgroup>
						</select>
					</div>
					<div class="input-field col s6">
						<select name="ano" class="browser-default" required>
                                                    <optgroup label="Ano">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </optgroup>
						</select>
					</div>
				</div>
                                <div class="row">
                                    <div class="col s12">
                                        <?php
                                        $erroCadastro = "";
                                        if(!empty($_SESSION['erroCadastro'])) {
                                            $erroCadastro = $_SESSION['erroCadastro'];
                                        }
                                        echo($erroCadastro);
                                        unset($_SESSION['erroCadastro']);
                                        ?>
                                    </div>
                                 </div>
				
				<div class="row center"> <button class="button-index btn-large waves-effect waves-light grey darken-1" type="submit">Cadastrar</button> </div>
			</form>
		</div>
	</div>
</body>
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/materialize.js"></script>

</html>