<?php 
session_start();
if(isset($_SESSION['idLogin'])){
    header("Location: home.php");
}
?>
<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
    <title>Bem-vindo</title>
    <link rel="stylesheet" href="css/materialize.css">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> </head>

<body class="light-green lighten-5">
    <header>
        <nav class="light-green lighten-1" role="navigation">
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo center"><img class="logo" src="img/ifrs-logo.svg"></a>
            </div>
        </nav>
    </header>
    <div class="row center">
        <h4 class="header col s12 light center">Descubra quanto falta para você atingir a média!</h4>
        <div class="center-div">
            <div class="col s12"><a href="login.php" class="button-index btn-large waves-effect waves-light grey darken-1">Login</a></div>
            <div class="col s12"><a href="cadastro.php" class="button-index btn-large waves-effect waves-light grey darken-1">Cadastro</a></div>
        </div>
    </div>
</body>
<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/materialize.js"></script>

</html>