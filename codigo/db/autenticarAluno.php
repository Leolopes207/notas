<?php
include_once "medoo.php";
include "Aluno.php";
$database = new medoo([
    'database_type' => 'pgsql',
    'database_name' => 'AppNotas',
    'server' => 'webacademico.canoas.ifrs.edu.br',
    'username' => 'leolopes',
    'password' => 'leo',
    'charset' => 'utf8'
]);
$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$senha = md5(filter_input(INPUT_POST, 'senha'));

$idAluno = $database->select("aluno", "id_aluno", [
    "AND" => [
        "email_aluno" => $email,
        "senha_aluno" => $senha
    ]
]);

if($idAluno == null) {
    session_start();
    $_SESSION['erro'] = 'Email/senha incorretos!';
    header("Location: ../login.php");
} else {
    session_start();
    $_SESSION['idLogin']= $idAluno[0];
    echo($_SESSION['idLogin']);
    header("Location: ../home.php");
}

?>


