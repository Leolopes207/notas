<?php

include_once('medoo.php');

class Aluno {

    private $nomeAluno;
    private $emailAluno;
    private $senhaAluno;
    private $idTurma;

    function __construct($nomeAluno, $emailAluno, $senhaAluno, $idTurma) {
        $this->nomeAluno = $nomeAluno;
        $this->emailAluno = $emailAluno;
        $this->senhaAluno = $senhaAluno;
        $this->idTurma = $idTurma;
    }

    function saveOnDb() {
        $database = new medoo([
            'database_type' => 'pgsql',
            'database_name' => 'AppNotas',
            'server' => 'webacademico.canoas.ifrs.edu.br',
            'username' => 'leolopes',
            'password' => 'leo',
            'charset' => 'utf8'
        ]);
        $newInstance = [
            "nome_aluno" => $this->nomeAluno,
            "email_aluno" => $this->emailAluno,
            "senha_aluno" => $this->senhaAluno,
            "id_turma" => $this->idTurma
        ];
        $database->insert("aluno", $newInstance);
        $database = null;
    }

    public static function loadAluno($idAluno) {
        $database = new medoo([
            'database_type' => 'pgsql',
            'database_name' => 'AppNotas',
            'server' => 'webacademico.canoas.ifrs.edu.br',
            'username' => 'leolopes',
            'password' => 'leo',
            'charset' => 'utf8'
        ]);
        
        $dados = $database->select("aluno", "*",[
            "id_aluno" => $idAluno
        ]);
        
        return $dados;
       
    }

    function getNomeAluno() {
        return $this->nomeAluno;
    }

    function getEmailAluno() {
        return $this->emailAluno;
    }

    function getSenhaAluno() {
        return $this->senhaAluno;
    }

    function getIdTurma() {
        return $this->idTurma;
    }

    function setNomeAluno($nomeAluno) {
        $this->nomeAluno = $nomeAluno;
        return $this;
    }

    function setEmailAluno($emailAluno) {
        $this->emailAluno = $emailAluno;
        return $this;
    }

    function setSenhaAluno($senhaAluno) {
        $this->senhaAluno = $senhaAluno;
        return $this;
    }

    function setIdTurma($idTurma) {
        $this->idTurma = $idTurma;
        return $this;
    }

}

?>