<?php
include_once "medoo.php";
include "Aluno.php";
$database = new medoo([
    'database_type' => 'pgsql',
    'database_name' => 'AppNotas',
    'server' => 'webacademico.canoas.ifrs.edu.br',
    'username' => 'leolopes',
    'password' => 'leo',
    'charset' => 'utf8'
]);
$nome = filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$senha = md5(filter_input(INPUT_POST, 'senha'));
$curso = filter_input(INPUT_POST, 'curso');
$ano = filter_input(INPUT_POST, 'ano');
$emailCadastrado = $database->select("aluno", "email_aluno", ["email_aluno" => $email]);
$idTurma = $database->select("turma", "id_turma", [
    "AND" => [
        "curso" => $curso,
        "ano" => $ano
    ]
]);
$idTurma = (int)$idTurma[0];
echo("<p>$nome $email $senha $idTurma $emailCadastrado[0]</p>");
if($emailCadastrado[0] == $email) {
    session_start();
    $_SESSION['erroCadastro'] = 'Email já cadastrado!';
    header("Location: ../cadastro.php");
} else {
    $aluno = new Aluno($nome, $email, $senha, $idTurma);
    $aluno->saveOnDb();
    
    $idAluno = $database->select("aluno", "id_aluno", [
    "AND" => [
        "email_aluno" => $email,
        "senha_aluno" => $senha
    ]
    ]);
    
    session_start();
    $_SESSION['idLogin']= $idAluno[0];
    echo($_SESSION['idLogin']);
    header("Location: ../home.php");
    
}

?>

