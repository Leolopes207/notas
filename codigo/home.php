<?php session_start(); 
if(empty($_SESSION['idLogin'])){
    header("Location: index.php");
} else {
    include "db/Aluno.php";
    $dados = Aluno::loadAluno($_SESSION['idLogin']);
}
?>
<!DOCTYPE HTML>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0">
        <title>Login</title>
        <link rel="stylesheet" href="css/materialize.css">
        <link rel="stylesheet" href="css/style.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> </head>

    <body class="light-green lighten-5">
        <header>
            
            <div class="navbar-fixed">
                
                <nav class="light-green lighten-1">
                    
                    <div class="nav-wrapper">
                    <ul id="slide-out" class="side-nav light-green lighten-5">
                        <li><a><span class="black-text"><?php echo("Olá, " . $dados[0]['nome_aluno'] . "!");?></span></a></li>
                        <li><a class="btn-large waves-effect waves-light grey darken-1" href="db/logoutAluno.php">Sair</a></li>
                    </ul>
                    <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>

                        <a href="#!" class="brand-logo center"><img class="logo" src="img/ifrs-logo.svg"></a>
                    </div>
                </nav>
            </div>

        </header>
        <div class="row">
            <div class="center-div">
                
                <?php
                echo($_SESSION['idLogin']); ?>
            </div>
        </div>
    </body>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script>
        $('.button-collapse').sideNav({
            menuWidth: 300, // Default is 240
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        }
        );
        $('.collapsible').collapsible();
    </script>

</html>